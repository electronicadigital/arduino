String stringDeCorte = "$#endStream#$";
  
void setup()
{
  Serial.begin(9600);
}
 
void loop()
{
  //Hay datos disponibles...
  if(Serial.available())
  {
    //Guarda los datos caracter a caracter.
    char charReceived = Serial.read(); 
 
    //Vamos poniendo cada caracter recibido en el array "command"
    
    //Cuando reciba un comando para obtener datos de los sensores, entra en el if.
    //En este caso, el comando es un salto de linea.
    if(charReceived == '\n')
    {
      //Escribimos el comando recibido en el Serial Monitor.
      Serial.print(charReceived);
      
      //Mandamos por Serial los datos que deberian obtenerse de los sensores. 
      Serial.println("Temperatura,Humedad del aire,Humedad del suelo");

      Serial.print(getTemperature());
      Serial.print(",");
      Serial.print(getHumidity());
      Serial.print(",");
      Serial.print(getSoilMoisture());
      Serial.println();

      Serial.println("°C,%,%");
      Serial.print(stringDeCorte);
    }
  }
}

float getTemperature(){
	return (float) random(10, 50);
}

float getHumidity(){
	return (float) random(10, 100);
}

float getSoilMoisture(){
	return (float) random(0,80);
}