// DHT
#include "DHT.h"
DHT dht;
int dhtDataPin = 10;

// YL 69
int yl69PinDeAlimentacion = A5;
int yl69PinDeDatos = A3;

String stringDeCorte = "$#endStream#$";
  
void setup()
{
	pinMode(yl69PinDeAlimentacion, OUTPUT);
  	dht.setup(dhtDataPin); // data pin 2
  	Serial.begin(9600);
}
 
void loop()
{
  //Hay datos disponibles...
  if(Serial.available())
  {
    //Guarda los datos caracter a caracter.
    char charReceived = Serial.read(); 
 
    //Vamos poniendo cada caracter recibido en el array "command"
    
    //Cuando reciba un comando para obtener datos de los sensores, entra en el if.
    //En este caso, el comando es un salto de linea.
    if(charReceived == '\n')
    {
      //Escribimos el comando recibido en el Serial Monitor.
      Serial.print(charReceived);
      
      //Mandamos por bluetooth los datos que leemos.
      // Primero, la cabecera
      // El nombre de cada valor leido, separado por comas.
      Serial.println("Temperatura,Humedad del aire,Humedad del suelo");
      
      // Luego, los datos separados por comas
      // y finalizados con un salto de linea
      Serial.print(dht.getTemperature());
      Serial.print(",");
      Serial.print(dht.getHumidity());
      Serial.print(",");
      Serial.print(getSoilMoisture());
      Serial.println();
      
      // Luego, las unidades separadas por comas.
      // Y un salto de linea
      Serial.println("°C,%,%");
      
      // Y por ultimo, el string de corte.
      Serial.print(stringDeCorte);
    }
  }
}

float getSoilMoisture(){
	digitalWrite(yl69PinDeAlimentacion, HIGH);
	delay(20);
	int lectura = analogRead(yl69PinDeDatos);
	digitalWrite(yl69PinDeAlimentacion, LOW);
	return floatMap(lectura, 0, 1023, 100, 0);
}

float floatMap(int value, int fromLow, int fromUp, float toLow, float toUp){
	return (value - fromLow) * (toUp - toLow) / (fromUp - fromLow) + toLow;
}