#include "DHT.h"

DHT dht;

String stringDeCorte = "$#endStream#$";
  
void setup()
{
  dht.setup(2); // data pin 2
  Serial.begin(9600);
}
 
void loop()
{
  //Hay datos disponibles...
  if(Serial.available())
  {
    //Guarda los datos caracter a caracter.
    char charReceived = Serial.read(); 
 
    //Vamos poniendo cada caracter recibido en el array "command"
    
    //Cuando reciba un comando para obtener datos de los sensores, entra en el if.
    //En este caso, el comando es un salto de linea.
    if(charReceived == '\n')
    {
      //Escribimos el comando recibido en el Serial Monitor.
      Serial.print(charReceived);
      
      //Mandamos por bluetooth los datos que leemos.
      // Primero, la cabecera
      // El nombre de cada valor leido, separado por comas.
      Serial.println("Temperatura,Humedad");
      
      // Luego, los datos separados por comas
      // y finalizados con un salto de linea
      Serial.print(dht.getTemperature());
      Serial.print(",");
      Serial.print(dht.getHumidity());
      Serial.println();
      
      // Luego, las unidades separadas por comas.
      // Y un salto de linea
      Serial.println("°C,%");
      
      // Y por ultimo, el string de corte.
      Serial.print(stringDeCorte);
    }
  }
}
