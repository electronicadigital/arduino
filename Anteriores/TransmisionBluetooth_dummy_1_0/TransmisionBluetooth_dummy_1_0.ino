#include <SoftwareSerial.h> 
 
//Se crea el objeto SoftwareSerial con los parametros de los pins RX y TX del modulo Bluetooth.
SoftwareSerial bluetooth(10,11); //10 RX, 11 TX.

String stringDeCorte = "$#endStream#$";
  
void setup()
{
  bluetooth.begin(9600);
  Serial.begin(9600);
}
 
void loop()
{
  //Hay datos disponibles...
  if(bluetooth.available())
  {
    //Guarda los datos caracter a caracter.
    char charReceived = bluetooth.read(); 
 
    //Vamos poniendo cada caracter recibido en el array "command"
    
    //Cuando reciba un comando para obtener datos de los sensores, entra en el if.
    //En este caso, el comando es un salto de linea.
    if(charReceived == '\n')
    {
      //Escribimos el comando recibido en el Serial Monitor.
      Serial.print(charReceived);
      
      //Mandamos por bluetooth los datos que deberian obtenerse de los sensores. 
      bluetooth.println("Temperatura,Humedad");
      bluetooth.println("22.00,88.00");
      bluetooth.println("°C,%");
      bluetooth.print(stringDeCorte);
    }
  }
}
